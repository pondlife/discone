#!/bin/sh

# -- includes --
. ./bin/core/builds.sh

# -- constants --
# subpath to the mac binary
TARGET_PATH_MAC="mac/discone.app"

# subpath to the windows binary
TARGET_PATH_WIN="win/discone.exe"

# -- parsing --
Usage() {
  echo "usage: ./bin/pt [-h]"
  exit -1
}

while getopts ":h" option; do
  case "${option}" in
    h*) Usage ;;
  esac
done

# -- commands --
# run the playtest build
RunBuild() {
  echo "- running build '$build'"

  # get target path
  case "$os" in
    mac) target="$TARGET_PATH_MAC" ;;
    win) target="$TARGET_PATH_WIN" ;;
  esac

  # make binary path
  binary="$build/$target"
  if [ -z "$target" ] || [ -z "$binary" ]; then
    echo "✘ missing $os binary: $target"
    exit 3
  fi

  # run the build
  open "$binary"
}

# -- queries --
# find the current os
FindOs() {
  case "$OSTYPE" in
    darwin*) os="mac" ;;
    *)       os="win" ;;
  esac
}

# -- main --
Main() {
  # TODO: don't hardcode this
  FindOs

  # find the most recent build
  FindBuild "$VARIANT_PLAYTEST"

  # and then run it
  RunBuild
}

Main "$*"