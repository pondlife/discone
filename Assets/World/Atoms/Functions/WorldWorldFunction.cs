namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `World`. Inherits from `AtomFunction&lt;World, World&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class WorldWorldFunction : AtomFunction<World, World> { }
}
