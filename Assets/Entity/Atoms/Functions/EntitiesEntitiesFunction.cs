namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `Entities`. Inherits from `AtomFunction&lt;Entities, Entities&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class EntitiesEntitiesFunction : AtomFunction<Entities, Entities> { }
}
